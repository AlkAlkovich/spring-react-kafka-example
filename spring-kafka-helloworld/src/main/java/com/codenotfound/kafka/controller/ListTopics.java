package com.codenotfound.kafka.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeoutException;



@RequiredArgsConstructor
public class ListTopics {

    private final ConsumerFactory<String, String> consumerFactory;

    public static void main(String[] args) throws IOException, TimeoutException, InterruptedException {

//        String process=new ProcessExecutor().command(
//                "/home/anikitin/workspace/utils/kafka_2.11-2.0.0/bin/kafka-topics.sh --list --zookeeper localhost:2181")
//                .redirectError(Slf4jStream.of(ListTopics.class).asInfo())
//                .readOutput(true).execute()
//                .outputUTF8();;


//        Process process=new ProcessBuilder()
//                .command("/home/anikitin/workspace/utils/kafka_2.11-2.0.0/bin/kafka-topics.sh --list --zookeeper localhost:2181").start();

//        String[] cmd = { "sh", "kafka-topics.sh --list --zookeeper localhost:2181", "/home/anikitin/workspace/utils/kafka_2.11-2.0.0/bin/"};
//
//        Process process=Runtime.getRuntime().exec(cmd);
//
//
//        System.out.println(convertStreamToString(process.getInputStream()));

//        getTopicsName();
    }

    static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }


    public ArrayList<String> getTopicList() {

        return new ArrayList<>(consumerFactory.createConsumer().listTopics().keySet());
    }

    public ArrayList<String> getTopicsName() {


//        ArrayList<String> nameTopics = new ArrayList<>();
//        try {
//            Process proc = Runtime.getRuntime().exec("/home/anikitin/workspace/utils/kafka_2.11-2.0.0/bin/kafka-topics.sh --list --zookeeper localhost:2181"); //Whatever you want to execute
//            BufferedReader read = new BufferedReader(new InputStreamReader(
//                    proc.getInputStream()));
//            try {
//                proc.waitFor();
//            } catch (InterruptedException e) {
//                System.out.println(e.getMessage());
//            }
//            while (read.ready()) {
//                System.out.print("topic name is ");
//                System.out.println(read.readLine());
//
//                nameTopics.add(read.readLine());
//
//            }
//        } catch (IOException e) {
//            System.out.println(e.getMessage());
//        }
//
//        return nameTopics

        return getTopicList();

    }
}
