package com.codenotfound.kafka.controller;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@JsonIgnoreProperties

@Data
public class SendRequest {

    private String topic;
    private String payLoad;
}
