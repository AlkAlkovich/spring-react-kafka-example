package com.codenotfound.kafka.controller;


import com.codenotfound.kafka.producer.JsonFromIntegration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class TestController {


    @RequestMapping(value = "/api/testJ", method = RequestMethod.POST)
    public void test(@RequestBody JsonFromIntegration jsonFromIntegration) {

        log.info("TEST {}", jsonFromIntegration);

    }
}
