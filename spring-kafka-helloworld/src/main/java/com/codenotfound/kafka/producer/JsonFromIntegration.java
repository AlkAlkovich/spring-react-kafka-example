package com.codenotfound.kafka.producer;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonFromIntegration {

    private String documentTypeId;
//    private UUID dataStoreId;
//    private UUID parentId;
//    private UUID userId;
//    private String formatData;


    public JsonFromIntegration() {
    }

    public JsonFromIntegration(String documentTypeId) {
        this.documentTypeId = documentTypeId;

    }

    public String getDocumentTypeId() {
        return documentTypeId;
    }

    public void setDocumentTypeId(String documentTypeId) {
        this.documentTypeId = documentTypeId;
    }



    //
//
//    public String getDocumentTypeId() {
//        return documentTypeId;
//    }
//
//    public void setDocumentTypeId(String documentTypeId) {
//        this.documentTypeId = documentTypeId;
//    }

//    public Byte[] getFileDocument() {
//        return fileDocument;
//    }
//
//    public void setFileDocument(Byte[] fileDocument) {
//        this.fileDocument = fileDocument;
//    }
}
