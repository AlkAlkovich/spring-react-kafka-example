package com.codenotfound.kafka.producer;

import com.codenotfound.kafka.controller.KafkaSendController;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import ru.pfr.nvpsv.oir.documents.application.avro.JsonFromIntegration;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.UUID;

public class Sender {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(Sender.class);

    @Autowired
    private KafkaTemplate<String, JsonFromIntegration> kafkaTemplate;

    public void send(String topic, String payload) throws IOException {

        InputStream stream = KafkaSendController.class.getResourceAsStream("/ПФР_034002_ЗРСПН_20151104_d36a4be1-8fd5-41fa-9834-c950df5192a2.xml");

        ByteBuffer byteBuffer = ByteBuffer.wrap(IOUtils.toByteArray(stream));


        JsonFromIntegration jsonFromIntegration = JsonFromIntegration
                .newBuilder()
                .setDocumentTypeId(UUID.randomUUID().toString())
                .setDataStoreId(UUID.randomUUID().toString())
                .setFormatData("XML")
                .setParentId(UUID.randomUUID().toString())
                .setUserId(UUID.randomUUID().toString())
                .setFileDocument(Arrays.asList(byteBuffer))
                .build();

//        ObjectMapper objectMapper = new ObjectMapper();
//        JsonFromIntegration jsonFromIntegration = objectMapper.convertValue(payload, JsonFromIntegration.class);
        LOGGER.info("sending payload='{}' to topic='{}'", jsonFromIntegration, topic);
        kafkaTemplate.send(topic, jsonFromIntegration);
    }
}
