package com.codenotfound.kafka.react;

import com.codenotfound.kafka.controller.ListTopics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Benjamin Winterberg
 */
@RestController
@RequestMapping("/comments.json")
public class CommentController {

    private CommentService service;
    private ListTopics listTopics;

    @Autowired
    public CommentController(CommentService service, ListTopics listTopics) {
        this.service = service;
        this.listTopics = listTopics;
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Comment> getComments() {
//        List<Comment> comments = listTopics
//                .getTopicsName()
//                .stream()
//                .map(s -> new Comment(" topic name",s+" "))
//                .collect(Collectors.toList());

      return   service.getComments();
//        return comments;
    }

    @RequestMapping(method = RequestMethod.POST)
    public List<Comment> addComment(Comment comment) {
        return service.addComment(comment);
    }

}
