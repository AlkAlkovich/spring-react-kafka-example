package com.codenotfound.kafka.react;

import com.codenotfound.kafka.controller.ListTopics;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.ContextStartedEvent;
import org.springframework.context.event.EventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

//@Service
@Slf4j
public class CommentService  {


    @Autowired
    private ListTopics listTopics;
    private List<Comment> comments = new ArrayList<>();

    @EventListener
    public void handlerContextStartedEvent(ContextRefreshedEvent event) {
        log.info("ContextStartedEvent TEST");
        comments.addAll(listTopics
                .getTopicsName()
                .stream()
                .map(s -> new Comment(" topic name", s + " "))
                .collect(Collectors.toList()));

    }

//    public CommentService() {
//        comments.add(new Comment("Peter Parker", "This is a comment."));
//        comments.add(new Comment("John Doe", "This is *another* comment."));
//    }

    public List<Comment> getComments() {
        comments.addAll(listTopics
                .getTopicsName()
                .stream()
                .map(s -> new Comment(" topic name", s + " "))
                .collect(Collectors.toList()));
        return comments;
    }

    public List<Comment> addComment(Comment comment) {
        comments.add(comment);
        return comments;
    }
}
