'use strict';

// tag::vars[]
const React = require('react');
const ReactDOM = require('react-dom');
const client = require('./client');
const jquery = require('jquery');
// const Bootstrap = require('bootstrap');
import 'bootstrap/dist/css/bootstrap.css';


// import 'bootstrap/dist/css/bootstrap.min.css';
// const EventEmitter =require('events');
// end::vars[]

// tag::app[]


class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            employees: [],
            count: 'test'
        };
    }

    componentDidMount() {
        jquery.ajax({
            url: '/comments.json',
            dataType: 'json',
            success: function (data) {
                this.setState({employees: data});
            }.bind(this),
            error: function (xhr, status, err) {
                console.error('/comments.json', status, err.toString());
            }.bind(this)
        });
    }

    render() {
        return (
            <div className="container">
                <EmployeeList callbackFromParent={(count) => this.setState({count})}
                              employees={this.state.employees}/>
                <Counter count={this.state.count}/>
            </div>

        )
    }
}

// end::app[]

class Counter extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            payLoad: ''
        };

        this.handleChangePayLoad = this.handleChangePayLoad.bind(this);
        this.sendData = this.sendData.bind(this);
    }

    handleChangePayLoad(event) {
        console.info(event.target.value);
        this.setState({payLoad: event.target.value});
    }

    sendData() {
        jquery.ajax({
            url: "/api/employee",
            dataType: 'json',
            type: 'POST',
            data: {
                author: this.props.count,
                text: this.state.payLoad
            },
            success: function (data) {
                console.info("success");
            }.bind(this),
            error: function (xhr, status, err) {
                console.error("notsuccess");
            }.bind(this)
        });
    }

    render() {
        const {count} = this.props;
        return (
            <h3>
                <span className="label label-info">Current topic for send: {count}</span>
                <form>
                    <div className="form-group">
                        {/*<label htmlFor="comment"></label>*/}
                        <textarea className="form-control" rows="5" id="comment"
                                  onChange={this.handleChangePayLoad}></textarea>
                        <button onClick={this.sendData}>test</button>
                    </div>
                </form>
            </h3>

        )
    }
}

// tag::employee-list[]
class EmployeeList extends React.Component {


    render() {
        const {callbackFromParent} = this.props;
        var employees = this.props.employees.map(employee =>
            <Employee callbackFromParent={callbackFromParent}
                      key={employee.text} employee={employee}/>
        );
        return (
            <div className="container">
                <h1>List topics</h1>
                <ul className="list-group">
                    {employees}
                </ul>
            </div>

        )
    }
}

// end::employee-list[]

// tag::employee[]
class Employee extends React.Component {


    render() {
        const {callbackFromParent} = this.props;
        return (
            <li className="list-group-item list-group-item-info"
                onClick={() => callbackFromParent(this.props.employee.text)}>
                {this.props.employee.text}
            </li>
        )
    }
}

// end::employee[]

// tag::render[]
ReactDOM.render(
    <App/>,
    document.getElementById('react')
)
// end::render[]

